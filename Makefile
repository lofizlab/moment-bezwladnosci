.PHONY: clean
clean:
	rm -fr auto *.aux *.fdb_latexmk *.fls *.log *.out *.pdf *.sta *.xdv
