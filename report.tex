\documentclass{article}

% Obecnie jedynie XeLaTeX w pełni wspiera Unicode, zaś LaTeX wymaga
% specjalnych czcionek. Jeśli chcesz jednak nadal korzystać z LaTeX,
% od-komentuj poniższa linię.
% \usepackage[T1]{fontenc}

% Ustawienia dla języka polskiego
\usepackage[polish]{babel}
\usepackage{indentfirst}

% Ustawienia strony
\usepackage{geometry}
\geometry{a4paper}

% Rozmieszczenie elementów
\usepackage{float}
\usepackage[justification=centering,margin=1.5cm]{caption}

% Grafiki i ilustracje
\usepackage[subpreambles=true]{standalone}
\usepackage{xcolor}

% Matematyka
\usepackage{amsmath}
\usepackage{siunitx}

% Formatowanie
\usepackage[unicode]{hyperref} % linki i spis treści
\usepackage{csquotes}
\usepackage{booktabs}

% Narzędzia pomocnicze
\usepackage{lipsum}

% Tytuł
\title{\normalsize Doświadczenie 10.\\
  \vspace{0.1in}
  \Large Badanie wahadła fizycznego i zjawiska bezwładności w ruchu obrotowym na przykładzie niestandardowej bryły sztywnej.
  \vspace{0.1in}
}
\author{Mateusz Ciszewski \and Piotr Kędzior \and Adam Maleszka}
\date{8 czerwca 2022}

\begin{document}

\maketitle

\section{Wstęp}

Złożoność świata sprawia, że jego całkowite ujęcie w formie uniwersalnych i systematycznych zasad nie jest możliwe bez użycia uproszczeń.
W mechanice punktu materialnego, zamiast rozpatrywać dokładne ruchy pojedynczych atomów, bada się więc zachowanie punktów materialnych.
Przyjmuje się wówczas, że cały ruch ciała fizycznego sprowadza się do ruchu \textit{środka masy} tegoż ciała.

Często jednak uproszczenie to bywa niemożliwe, gdy nie tylko masa, lecz także \textit{kształt} ciała fizycznego ma znaczenie.
Większość zależności okazuje się mimo to analogiczna: za ciało, nazywane \textit{bryłą sztywną}, uznaje się zbiór \textit{punktów masowych}, które posiadają swą masę, prędkość, pęd, energię kinetyczną, itd.

Podejście to jest szczególnie przydatne przy ruchu obrotowym, zachodzi wtedy proporcjonalność zwana \textbf{drugą zasadą dynamiki dla ruchu obrotowego}:
\[ \vec{M} = I \vec{\varepsilon} \]
gdzie:
\begin{description}
  \item[$M$] --- moment siły,
  \item[$\varepsilon$] --- przyspieszenie kątowe,
  \item[$I$] --- \textbf{moment bezwładności}.
\end{description}

Należy zauważyć podobieństwo do drugiej zasady dynamiki dla ruchu postępowego, zwanej też \textit{zasadą bezwładności}, gdzie moment siły to siła wypadkowa, przyspieszenie kątowe to przyspieszenie, a moment bezwładności to analogia masy.
O ile jednak masa stanowi oczywisty parametr fizyczny, moment bezwładności nie wydaje się na tyle intuicyjny wprost:
\[ \text{dla $n$ punktów masowych odległych o $r_i$ od środka masy:} \indent I = \sum_{i = 1}^{n} m_i r_i^2 \]

Dlatego też niniejsza praca ma na celu zbadanie przedstawionego zjawiska, w tym przypadku, przez obliczenie momentu bezwładności podanej bryły sztywnej. 
Warto zauważyć, że obliczenie momentu bezwładności sfery, prostopadłościanu, czy cylindra w istocie sprowadza się do podstawowego problemu z dziedziny analizy matematycznej.
Aby jednak wymusić poszukiwanie bardziej elementarnych metod, badaniom podlegać będzie mniej standardowa bryła sztywna --- papryka.

\section{Wstęp teoretyczny}

Przy znajomości podanej powyżej drugiej zasady dynamiki dla ruchu obrotowego najprostszym, i przy tym najlepszym, sposobem obliczenia momentu bezwładności wydaje się pomiar momentu siły i przyspieszenia kątowego, wtedy jedynie $I = \frac{\vec{M}}{\vec{\varepsilon}}$. Przykładową realizacją podobnego układu byłaby, chociażby, bryła sztywna z osią obrotu przechodzącą przez jej środek masy, na którą nawinięto nić z zawieszonym ciężarkiem. Wtedy $M = F_g r$, zaś $\varepsilon$ może zostać zmierzona przy użyciu rejestratora cyfrowego.

Okazuje się jednak, że nie jest wymagane, by oś obrotu przechodziła przez środek masy bryły sztywnej.
\textbf{Twierdzenie Steinera} umożliwia bowiem obliczenie momentu bezwładności względem osi przechodzącej przez środek masy na podstawie momentu bezwładności względem \textit{innej, równoległej osi obrotu}:
\[ I_S = I_O - md^2 \]
\begin{description}
  \item[$I_S$] --- moment bezwładności względem osi przechodzącej przez \textit{środek masy},
  \item[$I_O$] --- moment bezwładności względem osi równoległej do pierwszej osi,
  \item[$d$] --- odległość między osiami,
  \item[$m$] --- masa bryły.
\end{description}

Możliwość przyjęcia osi obrotu nieprzechodzącej przez środek masy umożliwia zbadanie rozpatrywanej bryły sztywnej za pomocą \textbf{wahadła fizycznego}.
Rozważmy wahadło fizyczne przedstawione na poniższej ilustracji i na potrzeby doświadczenia przyjmijmy oznaczenia jak na rysunku:

\begin{figure}[H]
  \begin{center}
    \include{figures/pendulum}
    \caption{Wahadło fizyczne z osią obrotu $O$ na podstawie bryły sztywnej z środkiem masy $S$, gdzie $Q$ to siła ciążenia bryły sztywnej o masie $m$; dla niewielkiego $\alpha$: $x_0 \approx x$.}
  \end{center}
\end{figure}

Istotnie, mamy:
\begin{align}
I_O\varepsilon &= M \label{eq:1} \\
\varepsilon &= \frac{a}{r} \label{eq:2} \\
M &= mgr\sin{\alpha} = mgx_0 \label{eq:3}
\end{align}
Zauważmy, że $x_0 \approx x$, bowiem:
\[ x_0 = r\sin{\alpha} \approx x = 2\pi r \cdot \frac{\alpha}{2\pi} = r \alpha \]
czyli:
\[ \sin{\alpha} \approx \alpha \]
co jest natychmiastowym wnioskiem z wzorów Taylora.

Podstawmy równania~\ref{eq:2} i~\ref{eq:3} do równania~\ref{eq:1} z uwzględnieniem, że $x_0 \approx x$:
\[ a = \frac{mgr}{I_O}x \]

Z powyższego równania wynika, że $F$ jest proporcjonalne do $x$, a więc bryła sztywna porusza się \textbf{ruchem harmonicznym}.
Uwzględnić zatem możemy \textit{częstość kołową} (tzw.\ pulsację), przy której:
\[ a = \omega^2x \indent \text{oraz} \indent \omega = \frac{2\pi}{T} \]
Otrzymujemy układ równań:
\begin{equation}
  \left\{
    \begin{aligned}
      \omega^2x &= \frac{mgr}{I_O}x \\
      \omega &= \frac{2\pi}{T}
    \end{aligned}
  \right.
\end{equation}
którego rozwiązaniem dla $I_O$ jest:
\[ I_O = \frac{T^2mgr}{4\pi^2} \]
a jako, że $I = I_O -mr^2$:
\begin{equation}\label{eq:5}
I = mr \left( \frac{T^2g}{4\pi^2} - r \right)
\end{equation}

Powyższa równość może być wykorzystana, by z \textit{dużą dokładnością} przybliżyć moment bezwładności każdej bryły sztywnej.
Choć błędem pomiarowym obarczone są dwie wartości: $T$ oraz $r$, należy pamiętać, że urządzenia pomiaru odległości są dosyć precyzyjne, a jako że $T$ występuje w wielomianie drugiego stopnia, dla niewielkich kątów $\alpha$ i $T < 1\si{\second}$ niepewność $T$ będzie miała niewielki wpływ, bowiem $T^2 < T$:

\begin{figure}[H]
  \begin{center}
    \include{plots/square_vs_linear}\vspace{-0.3in}
    \caption{Porównanie okresu ($T$) liniowego i w wielomianie drugiego stopnia}\label{fig:square_vs_linear}
  \end{center}
\end{figure}

\section{Schemat doświadczalny}

Zgodnie z otrzymaną równością~\ref{eq:5} dla wahadła fizycznego, moment bezwładności bryły sztywnej wyliczony może być w oparciu o następujące pomiary:

\begin{description}
  \item[$m$] --- masę rozpatrywanej bryły,
  \item[$r$] --- odległość między środkiem masy ($S$), a osią obrotu ($O$),
  \item[$T$] --- okres drgań wahadła fizycznego.
\end{description}

Oprócz konstrukcji samego wahadła fizycznego, należy również zmierzyć \textit{masę} badanej bryły sztywnej oraz zaznaczyć \textit{środek masy} bryły, by następnie --- po konstrukcji wahadła --- uzyskać pomiar $r$.
Masę bryły zmierzyć można dowolną wagą klasyczną, zaś odległość $r$ --- linijką po zaznaczeniu środka masy wybranym sposobem.
W przypadku niniejszej realizacji została użyta waga spożywcza o niepewności pomiarowej $\Delta m = 0.01\si{\gram}$ oraz linijka szkolna przy przyjęciu nieco zawyżonej niepewności pomiarowej $\Delta r = 1\si{\centi\meter}$.

\subsection{Pozyskiwanie środka masy}

Przed rozpoczęciem konstruowania wszelkiego wahadła fizycznego jest to niezmiernie ważne, by zaznaczyć \textbf{środek masy} badanej bryły sztywnej.
W przypadku podstawowych figur geometrycznych zadanie to nie jest trudne, kłopotliwe zaś może się okazać w przypadku, na przykład, papryki.

Przy wyznaczaniu środka masy niestandardowej bryły sztywnej, warto wykorzystać działanie zjawisk naturalnych, na przykład: \textbf{ciążenia}.
Bryłę w miejscu innym niż środek masy należy swobodnie przymocować nicią do stabilnego stojaka jak na ilustracji poniżej:

\begin{figure}[H]
  \begin{center}
    \include{figures/stand}
    \caption{Niestandardowa bryła sztywna o środku masy $S$, zawieszona swobodnie nicią na stojaku; $r$ stanowi odległość między środkiem masy, a punktem przymocowania nici.}
  \end{center}
\end{figure}

Jako że bryła po zawieszeniu pozostaje w spoczynku, to:
\[ M = rF_N\cdot\sin{\alpha} = 0 \]
a ponieważ $r > 0$ oraz $F_N > 0$, to $\sin{\alpha}=0$.

Oznacza to, że wektory $\vec{F_N}$ oraz $\vec{r}$ są współliniowe, więc z całkowitą pewnością można stwierdzić, że środek masy zawiera się w tym pionie.
Dla dowolnej bryły zatem środek masy można uzyskać przez odnalezienie \textit{miejsca przecięcia pionów} otrzymanych z wielu różnych przymocowań danej bryły.

\subsection{Wahadło fizyczne}

Po przygotowaniu wszystkich niezbędnych pomiarów, należy przystąpić do wyznaczania okresu wahnięć bryły sztywnej przy \textbf{wahadle fizycznym}.
\textit{De facto}, wahadło fizyczne niewiele się różni od wahadła matematycznego; ważne jest jednak, by zamiast swobodnie przymocowanego ciężarka na nitce, bryłę sztywną stabilnie przymocować tak, by oś obrotu ($O$) przechodziła przez jej \textit{objętość} i była \textit{niezmienna względem układu odniesienia}.

Co z kolei jest wspólne dla obu tych wahadeł, należy zachowywać stosunkowo \textit{niewielki kąt} z racji na użycie tego samego przybliżenia: $\sin{\alpha} \approx \alpha$.
Choć jest to przybliżenie dość precyzyjne, bowiem dla $\alpha = 0.5\si{\radian} \approx 30\si{\degree}$ błąd stanowi zaledwie $4\si{\percent}$ (zobacz rysunek~\ref{fig:rad_vs_sin} poniżej), zaleca się przygotowanie dwóch oddzielnych zestawów pomiarów: z mniejszym oraz z większym kątem odchyleń, przy progu $\alpha \approx 30\si{\degree}$.
Pozwoli to na dokładne zbadanie wpływu tego przybliżenia.

\begin{figure}[H]
  \begin{center}
    \include{plots/rad_vs_sin}\vspace{-0.3in}
    \caption{Ilustracja przybliżenia $\sin{\alpha} \approx \alpha$ wynikającego z wzorów Taylora}\label{fig:rad_vs_sin}
  \end{center}
\end{figure}

Do pozyskiwania pomiarów okresu wahnięć rekomenduje się użycie rejestratora cyfrowego oferującego mniejsze niepewności pomiarowe czasu.
W tym przypadku była to kamera cyfrowa rejestrująca obraz z szybkości $30\si{\text{kl.}\per\second}$, zatem $\Delta T = \frac{1}{30} \si{\second}$.
Jako że drgania wahadła fizycznego podlegają tłumieniu z racji na opór elementów, wartość okresu należy uśrednić na podstawie trzech pierwszych wahnięć z każdego razu.

\section{Analiza pomiarów}

Po przeprowadzeniu doświadczenia, wartości charakteryzujące finalny układ fizyczny, takie jak masa bryły sztywnej, czy odległość osi obrotu od środka masy, przedstawiono poniżej:

\begin{figure}[H]
  \begin{center}
    \begin{tabular}{ccc}
      \toprule
      Parametr & Wartość & Niepewność \\
      \midrule
      $m [\si{\kilo\gram}]$ & $0.15887$ & $0.00001$ \\
      $r [\si{\meter}]$ & $0.04$ & $0.01$ \\
      $T [\si{\second}]$ &     & $\frac{1}{30}$ \\
      \bottomrule
    \end{tabular}
    \caption{Założone wartości charakteryzujące użyty układ fizyczny.}\label{fig:assumptions}
  \end{center}
\end{figure}

Dokładne zarejestrowanie ruchu harmonicznego bryły sztywnej umożliwiło uzyskać pomiary okresu wahnięć, spisane poniżej:

\begin{figure}[H]
  \begin{center}
    \include{tables/data1}
    \caption{Okresy pierwszych wahnięć wahadła fizycznego dla zestawu pierwszego (z małym kątem odchylenia $\alpha$).}\label{fig:data1}
  \end{center}
\end{figure}

\begin{figure}[H]
  \begin{center}
    \include{tables/data2}
    \caption{Okresy pierwszych wahnięć dla zestawu drugiego (z dużym kątem odchylenia $\alpha$) --- część pierwsza.}\label{fig:data2}
  \end{center}
\end{figure}

\begin{figure}[H]
  \begin{center}
    \include{tables/data2_con}
    \caption{Okresy pierwszych wahnięć dla zestawu drugiego (z dużym kątem odchylenia $\alpha$) --- część wtóra.}\label{fig:data2_con}
  \end{center}
\end{figure}

Należy zwrócić uwagę, że wszystkie otrzymane pomiary, niezależnie od zestawu, są bardzo \textbf{zbliżone}.
Istotnie, z równości~\ref{eq:5} wynika, że skoro $I=\text{const.}$ dla danej bryły sztywnej, to okres wahnięć powinien być taki sam i \textit{niezależny od kąta odchylenia $\alpha$}:

\begin{equation}\label{eq:constancy}
I = mr \left( \frac{T^2g}{4\pi^2} - r \right) = \text{const.} \implies T = \text{const.}
\end{equation}

Wygląda jednak na to, że pomiary z zestawu drugiego (a więc z większym kątem odchylenia~$\alpha$) są średnio większe:
\[ T_{1\text{śr}} \approx 0.496 < T_{2\text{śr}} \approx 0.541 \]

Co prawda, różnica między tymi średnimi ($T_{2\text{śr}} - T_{1\text{śr}} \approx 0.045$) jest niewiele większa od niepewności pomiarowej.
Wciąż jednak, wzrost okresu wahnięć wobec kąta odchylenia wydaje się zjawiskiem obejmującym zdecydowaną większość pomiarów z zestawu drugiego.

\begin{figure}[H]
  \begin{center}
    \include{tables/calculations1}
    \caption{Obliczony moment bezwładności na podstawie pomiarów z doświadczenia --- zestaw pierwszy (z małym kątem odchylenia $\alpha$).}\label{fig:calculations1}
  \end{center}
\end{figure}

\begin{figure}[H]
  \begin{center}
    \include{tables/calculations2}
    \caption{Obliczony moment bezwładności na podstawie pomiarów z doświadczenia --- zestaw drugi (z dużym kątem odchylenia $\alpha$).}\label{fig:calculations2}
  \end{center}
\end{figure}

Wpływ ów zjawiska jeszcze bardziej uwydatniają obliczone wartości $I$ (zobacz tabele~\ref{fig:calculations1} i~\ref{fig:calculations2}), gdzie $I_1 \approx 0.000134 < I_2 \approx 0.000208$.
Po posortowaniu wszystkich pomiarów $T$ niemalejąco, tę niemalże liniową zależność jeszcze bardziej wykazuje poniższy wykres:

\begin{figure}[H]
  \begin{center}
    \include{plots/moment}\vspace{-0.3in}
  \end{center}
\end{figure}

Nietrudno stwierdzić, że powyższe nieprawidłowości wynikają z przybliżenia $\sin{\alpha} \approx \alpha$, które dla dokładności wymaga niewielkich kątów $\alpha$ (zobacz wykres~\ref{fig:rad_vs_sin}).
Spróbujmy zatem równanie~\ref{eq:5} sformułować bez użycia podanego przybliżenia; otrzymujemy układ równań:
\begin{equation}\label{eq:7}
  \left\{
    \begin{aligned}
      I_O\frac{a}{r} &= mgr\sin{\alpha} \\
      x &= r\alpha \\
      a &= \omega^2x \\
      \omega &= \frac{2\pi}{T} \\
      I &= I_O -mr^2 \\
    \end{aligned}
  \right.
\end{equation}
z którego mamy:
\begin{equation}\label{eq:8}
  I = \frac{T^2mgr}{4\pi^2}\cdot\frac{\sin{\alpha}}{\alpha} - mr^2
\end{equation}
zatem:
\[ T^2 \sim \frac{\alpha}{\sin{\alpha}} \]

Oznacza to więc, że w rzeczywistości okres wahnięć wahadła fizycznego jest w zupełności \textbf{zależny od kąta nachylenia}.
Niestety jednak równanie~\ref{eq:8} nie może posłużyć do dokładnego obliczenia momentu bezwładności, ponieważ układ równań~\ref{eq:7} wymaga ruchu harmonicznego, przy którym okres wahnięć jest stały.
Po drugie, równanie to wymaga znajomości kąta $\alpha$, co sprawia je niewygodnym i obarczonym dużym błędem pomiarowym przy samym doświadczeniu.

Jako wniosek należy pamiętać, że \textit{wahadło fizyczne nie wykonuje idealnego ruchu harmonicznego}, co wymusza zastosowanie uproszczeń i przybliżeń.
Nadal jednak, obliczenie momentu bezwładności z uwzględnieniem, że $\sin{\alpha} \approx \alpha$ pozwala uzyskać relatywnie bliskie przybliżenie.

\subsection{Analiza niepewności pomiarowej}

Analizę niepewności pomiarowej należy rozpocząć od obserwacji, że w przypadku równania~\ref{eq:5} nie jest możliwe zastosowanie metody Najmniej Korzystnych Przypadków (NKP) wprost:
\[ I = mr \left( \frac{T^2g}{4\pi^2} - r \right) \]
Wpływ na niepewność $I$ mają bowiem nie tylko $m$ i $T$, których zachowanie ma charakter przewidywalny i oczywisty, lecz także $r$, które po wymnożeniu przez nawias tworzy funkcję kwadratową:
\begin{equation}
  f(r) = -mr^2 + \frac{T^2gm}{4\pi^2}r
\end{equation}

W tym przypadku, metoda NKP sprowadza się do poszukiwania argumentu \textit{minimum} i \textit{maximum} funkcji $f(x)$ w dziedzinie $x \in [r-\Delta r; r+\Delta r]$ i następnego obliczenia $\Delta I$:
\begin{equation}
  \Delta I = \frac{f_{\text{max}}(x_{\text{max}}) - f_{\text{min}}(x_{\text{min}})}{2}
\end{equation}
gdzie:
\begin{align*}
  f_{\text{max}}(x) &= -(m + \Delta m)x^2 + \frac{(T + \Delta T)^2g(m + \Delta m)}{4\pi^2}x \\
  f_{\text{min}}(x) &= -(m - \Delta m)x^2 + \frac{(T - \Delta T)^2g(m - \Delta m)}{4\pi^2}x \\
\end{align*}

Na przykład, przy zestawie pierwszym (który \textit{notabene} pozwala uzyskać lepsze przybliżenie~$I$), dla $T_{\text{śr}} = 0.496 \pm \frac{1}{30}$ (jednostki SI), $m = 0.15887 \pm 0.00001$, $r = 0.04 \pm 0.01$, uzyskamy funkcje:

\begin{align*}
  f_{\text{max}}(x) &= -0.15888x^2 + 0.01106x \\
  f_{\text{min}}(x) &= -0.15886x^2 + 0.00845x \\
\end{align*}

\begin{figure}[H]
  \begin{center}
    \include{plots/minmax}
    \caption{Funkcje \textcolor{cyan}{$f_{\text{max}}(x)$} i \textcolor{green}{$f_{\text{min}}(x)$} z dziedziną $x \in [r-\Delta r; r+\Delta r]$ zaznaczoną adekwatnymi kolorami.}
  \end{center}
\end{figure}

Z powyższego wykresu mamy, że:
\[ \Delta I \approx \frac{(1.92 - 0.25) \cdot 10^{-4}}{2} = 0.0000835 [\si{\kilo\gram\meter\squared}] \]

Wykres ten również potwierdza, że --- istotnie --- wzory na NKP nie mogą być użyte, ponieważ argument $r-\Delta r$ nie jest \textit{maximum} funkcji $f_{\text{max}}(x)$.

Niech zatem $W[p,q]$ oznacza wektor wskazujący wierzchołek funkcji kwadratowej $f_{\text{max}}(x)$.
Zauważmy wtedy, że gdyby $p \le r - \Delta r$, to zastosowanie wzorów na metodę Najmniej Korzystnego Przypadku byłoby możliwe.

Zauważmy też, że funkcję $f_{\text{max}}(x)$ można zapisać w postaci ogólnej, gdzie $\vec{W}$ to wektor translacji:
\[ f_{\text{max}}(x) = ax^2 + bx + c = a(x - p)^2 + q = ax^2 + (-2ap)x + (ap^2 + q) \]
Z czego mamy, że:
\[ p = \frac{(T + \Delta T)^2 g}{8\pi^2} \le r - \Delta r \]

Oznacza to więc, że dla dowolnego wahadła fizycznego z osią obrotu o $r$ odległą od środka masy i dodatnimi niepewnościami $\Delta T$ i $\Delta r$ (gdzie $\Delta r \le r$ oraz $\Delta T \le T$), żeby skorzystać z wzorów na NKP, musi zachodzić:
\begin{equation}
  T \le \sqrt{\frac{8\pi^2(r-\Delta r)}{g}} - \Delta T
\end{equation}
co przy $r = 0.04 \pm 0.01$ i $\Delta T = \frac{1}{30}$:
\[ T \le 0.458 \]

Jest to szczególnie niefortunne, zwłaszcza, że najmniejsza średnia otrzymanych pomiarów okresu: $T_{\text{min}} \approx 0.478$.
Nie mniej jednak całość pozwala uzyskać moment bezwładności papryki: $I = 0.000134 \pm 0000835 [\si{\kilo\gram\meter\squared}]$.

\section{Wnioski}

Niniejsze doświadczenie pozwoliło zbadać zjawisko \textbf{bezwładności przy ruchu obrotowym} i dokładnie poznać właściwości i charakterystykę \textbf{wahadła fizycznego}.
Wahadło fizyczne okazało się bowiem efektywnym narzędziem, dzięki któremu --- przy wykorzystaniu twierdzenia Steinera i wzorów Taylora --- udało się oszacować \textbf{moment bezwładności} niestandardowej bryły sztywnej: $I = 0.000134 \pm 0000835 [\si{\kilo\gram\meter\squared}]$.

W trakcie doświadczenia szczególnie kłopotliwym okazał się fakt, że wahadło fizyczne \textit{nie wykonuje idealnego ruchu harmonicznego}.
Wymagane okazało się użycie \textbf{przybliżenia}: $\sin{\alpha} \approx \alpha$ będącego natychmiastowym wnioskiem z wzorów Taylora.

Na wynik znacznie większy wpływ od przybliżenia miały \textbf{niepewności pomiarowe}.
Osiągnięta niepewność wyniku była większa niż zakładano i wynosiła $\Delta I = 0.000835 \si{\kilo\gram\meter\squared}$, czyli około $62\si{\percent}$ wartości.
Wybrana bryła sztywna (papryka) okazała się problematyczna i wymagała \textit{zawyżenia niepewności pomiarowych} przyrządów badawczych, co prawdopodobnie było główną przyczyną niedokładnego wyniku.
Mimo wszystko, otrzymane pomiary okresu wahnięć wykazywały wysoką \textbf{powtarzalność i precyzję}.

Niestety, przy analizie niepewności, skorzystanie z wzorów metody Najmniej Korzystnych Przypadków, z racji na niebanalną naturę wahadła fizycznego, okazało się \textit{niemożliwe}.
Na szczęście jednak, po doborze odpowiednich funkcji kwadratowych, niepewność momentu bezwładności udało się wyliczyć, odnajdując \textit{maximum} i \textit{minimum} tych funkcji. 

Nie mniej jednak i bez żadnych wątpliwości, przeprowadzony eksperyment przybliżył zjawisko bezwładności oraz model wahadła fizycznego.

\end{document}